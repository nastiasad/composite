package by.sadovaya.textparse.entity;

import java.util.ArrayList;

/**
 * Created by Анастасия on 06.10.2015.
 */
public class Composite implements Component {
    private ArrayList <Component> components = new ArrayList<Component>();

    public Composite (ArrayList <Component> components){
        this.components = components;
    }

    public  Composite () {}

    @Override
    public void operation() {

    }

    @Override
    public void add(Component c) {
        components.add(c);
    }

    @Override
    public void remove(Component c) {
        components.remove(c);
    }

    @Override
    public Component getChild(int index) {
        if(index < components.size()) {
            return components.get(index);
        }
        else {
            return null;
        }
    }

    public int size(){
        return components.size();
    }

    public String toString (){
        StringBuilder sb = new StringBuilder();
        for (Component i: components){
            sb.append(i.toString());
        }
        return new String(sb);

    }
}
