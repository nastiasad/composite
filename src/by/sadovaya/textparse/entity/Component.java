package by.sadovaya.textparse.entity;

/**
 * Created by Анастасия on 06.10.2015.
 */
public interface Component {
    void operation ();

    //здесь можно убрать add, remove
    void add (Component c);
    void remove (Component c);
    Component getChild (int index);
}
