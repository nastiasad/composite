package by.sadovaya.textparse.entity;

/**
 * Created by Анастасия on 06.10.2015.
 */


//better return boolean

public class Leaf implements Component{
    public static enum Type {WORD, PUNCTUATION};

    private Type type;
    private String element;

    public Leaf (Type type, String element){
        this.type = type;
        this.element = element;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String toString() {
        return element;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public void operation() {
        System.out.println("Do something");
    }

    @Override
    public void add(Component c) {
        System.out.println("Do nothing");
    }

    @Override
    public void remove(Component c) {
        System.out.println("Do nothing");
    }

    @Override
    public Component getChild(int index) {
        System.out.println("Do nothing");
        return null;
    }
}
