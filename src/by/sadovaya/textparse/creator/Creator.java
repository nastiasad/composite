package by.sadovaya.textparse.creator;

import by.sadovaya.textparse.entity.Component;
import by.sadovaya.textparse.entity.Composite;
import by.sadovaya.textparse.entity.Leaf;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Анастасия on 07.10.2015.
 */
public class Creator {
    private static final Logger LOG = Logger.getLogger(Creator.class);

    public static String createTextString (String filePath){
        StringBuffer buf = new StringBuffer();
        try {
            Scanner sc = new Scanner(new File(filePath));
            while (sc.hasNextLine()){
                buf.append(sc.nextLine());
                buf.append('\n');
            }
        } catch (FileNotFoundException e) {
            LOG.fatal(e.getMessage());
            throw new RuntimeException();
        }
        String temp = new String(buf);
        Pattern pattern = Pattern.compile(" +");
        Matcher matcher = pattern.matcher(temp);
        temp = matcher.replaceAll(" ");
        return temp;
    }
}
