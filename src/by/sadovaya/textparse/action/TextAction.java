package by.sadovaya.textparse.action;

import by.sadovaya.textparse.entity.Component;
import by.sadovaya.textparse.entity.Composite;
import by.sadovaya.textparse.entity.Leaf;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Анастасия on 13.10.2015.
 */
public class TextAction {
    private static final Logger LOG = Logger.getLogger(TextAction.class);

    public ArrayList<String> sortSentences (Composite paragraphComp) {
        ArrayList<Pair> listPairs = new ArrayList<Pair>();
        ArrayList<String> listSentences = new ArrayList<String>();
        int index = 0;
        for (int i=0; i<paragraphComp.size(); i++){
            for (int j=0; j<((Composite)paragraphComp.getChild(i)).size(); j++) {
                int numWords = countWords((Composite) paragraphComp.getChild(i).getChild(j));
                listSentences.add(((Composite) paragraphComp.getChild(i).getChild(j)).toString());
                listPairs.add(new Pair(numWords, index));
                index++;
            }

        }
        Collections.sort(listPairs);
        ArrayList<String> sortedSentences = new ArrayList<String>();
        for (Pair i: listPairs){
            sortedSentences.add(listSentences.get(i.getIndex()).toString());
        }
        return sortedSentences;
    }

    public void changeWords (Composite paragraphComp) {
        for (int i=0; i<paragraphComp.size(); i++) {
            for (int j = 0; j < ((Composite) paragraphComp.getChild(i)).size(); j++) {
                change((Composite) paragraphComp.getChild(i).getChild(j));
            }
        }
    }

    private void change (Composite sentenceComp) {
        Leaf firstWord = (Leaf)sentenceComp.getChild(0).getChild(0);
        Leaf lastWord = (Leaf) sentenceComp.getChild(sentenceComp.size()-1).getChild(0);

        String temp = firstWord.getElement();
        firstWord.setElement(lastWord.getElement());
        lastWord.setElement(temp);
    }

    private int countWords (Composite composite){
        int res = 0;
        for (int i=0; i<composite.size(); i++){
            Component childComp = composite.getChild(i);
            if (childComp instanceof Leaf){
                if(((Leaf)childComp).getType() == Leaf.Type.WORD)
                    res++;
            } else {
                res += countWords((Composite) childComp);
            }
        }
        return res;
    }

    private class Pair implements Comparable<Pair>{
        private int numWords;
        private int index;

        Pair (int numWords, int index) {
            this. numWords = numWords;
            this.index = index;
        }

        public int getIndex (){
            return index;
        }

        @Override
        public int compareTo(Pair o) {
            return numWords - o.numWords;
        }
    }
}
