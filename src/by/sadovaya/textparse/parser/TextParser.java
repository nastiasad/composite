package by.sadovaya.textparse.parser;

import by.sadovaya.textparse.entity.Component;
import by.sadovaya.textparse.entity.Composite;
import by.sadovaya.textparse.entity.Leaf;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Анастасия on 08.10.2015.
 */
public class TextParser {
    private static final Logger LOG = Logger.getLogger(TextParser.class);

    private static final String PARAGRAPH_PAT = "[^\n].+[\n]";
    private static final String SENTENCE_PAT = "([^\\s])([^\\p{Punct}]+)([\\p{Punct}])\\s?";
            /*
            "[A-Z].+(\\. ?[a-z])?\\.*[\\p{Punct}]\\s?\\n?";
            */
    private static final String LEXEME_PAT =  "([^(\\s\\.|!|\\?|,|)]*)((\\s)+|(\\p{Punct}\\s?))";
    private static final String WORD_PUNCTUATION_PAT = "(\\w+)|(\\s)|(\\p{Punct}\\s?)";
    private static final String WORD_PAT = "\\w+";

    public Composite parseParagraph(String textString){
        Composite paragraphComposite = new Composite();
        Pattern p1 = Pattern.compile(PARAGRAPH_PAT);
        Matcher m1 = p1.matcher(textString);
        while (m1.find()){
            String foundString = m1.group();
            //System.out.println(foundString);
            paragraphComposite.add(parseSentence(foundString));
        }
        return paragraphComposite;
    }

    public Composite parseSentence(String textString){
        Composite sentenceComposite = new Composite();
        Pattern p1 = Pattern.compile(SENTENCE_PAT);
        Matcher m1 = p1.matcher(textString);
        while (m1.find()){
            String foundString = m1.group();
            //System.out.println(foundString);
            sentenceComposite.add(parseLexeme(foundString));
        }
        return sentenceComposite;
    }

    public Composite parseLexeme(String textString){
        Composite lexemeComposite = new Composite();
        Pattern p1 = Pattern.compile(LEXEME_PAT);
        Matcher m1 = p1.matcher(textString);
        while (m1.find()){
            String foundString = m1.group();
            //System.out.println(foundString);
            lexemeComposite.add(parseWordPunctuation(foundString));
        }
        return lexemeComposite;
    }

    public Composite parseWordPunctuation(String textString){
        Composite wordStopComposite = new Composite();
        Pattern p1 = Pattern.compile(WORD_PUNCTUATION_PAT);
        Matcher m1 = p1.matcher(textString);
        while (m1.find()){
            String foundString = m1.group();
            //System.out.println(foundString);
            Pattern tempPat = Pattern.compile(WORD_PAT);
            Matcher tempMatch = tempPat.matcher(foundString);
            if(tempMatch.matches()){
                wordStopComposite.add(new Leaf(Leaf.Type.WORD, foundString));
            } else {
                wordStopComposite.add(new Leaf(Leaf.Type.PUNCTUATION, foundString));
            }
        }
        return wordStopComposite;
    }
}
