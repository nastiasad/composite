package by.sadovaya.textparse.main;

import by.sadovaya.textparse.action.TextAction;
import by.sadovaya.textparse.creator.Creator;
import by.sadovaya.textparse.entity.Component;
import by.sadovaya.textparse.entity.Composite;
import by.sadovaya.textparse.parser.TextParser;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Анастасия on 06.10.2015.
 */
public class MainClass {
    static {
        new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
    }
    private static final String fileName = "text.txt";
    private static final Logger LOG = Logger.getLogger(MainClass.class);

    public static void main(String[] args) {
        String textString = Creator.createTextString(fileName);
        Composite paragraphComp = new TextParser().parseParagraph(textString);

        LOG.info("All the sentences of the text sorted in the number of words order.");
        ArrayList<String> sortedSentences = new TextAction().sortSentences(paragraphComp);
        for(String i: sortedSentences) {
            LOG.info(i);
        }

        new TextAction().changeWords(paragraphComp);
        LOG.info("The same text, but first and last words in each sentence swapped.");
        LOG.info(paragraphComp.toString());
    }
}
