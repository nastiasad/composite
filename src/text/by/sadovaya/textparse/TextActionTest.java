package text.by.sadovaya.textparse;

import by.sadovaya.textparse.action.TextAction;
import by.sadovaya.textparse.creator.Creator;
import by.sadovaya.textparse.entity.Composite;
import by.sadovaya.textparse.parser.TextParser;
import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by Анастасия on 13.10.2015.
 */
public class TextActionTest {

    @Test
    public void sortSentencesTest (){
        String textString = Creator.createTextString("text.txt");
        Composite paragraphComp = new TextParser().parseParagraph(textString);
        ArrayList<String> actual = new TextAction().sortSentences(paragraphComp);
        ArrayList<String> expected = new ArrayList<String>();
        fillListForSort(expected);
        Assert.assertEquals(actual, expected);
    }




    private void fillListForSort(ArrayList<String> expected) {
        expected.add("Hello. ");
        expected.add("My name is Nastia.\n");
        expected.add("I live in Minsk.\n");
    }
}
