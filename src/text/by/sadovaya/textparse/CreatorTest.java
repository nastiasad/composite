package text.by.sadovaya.textparse;

import by.sadovaya.textparse.creator.Creator;
import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by Анастасия on 13.10.2015.
 */
public class CreatorTest {
    @Test(expected=RuntimeException.class)
    public void createTextString (){
        String filePath = "abrakadabra.txt";
        String expected = createString();
        String actual = Creator.createTextString(filePath);
        Assert.assertEquals(actual, expected);
    }

    private String createString(){
        return "Hello. My name is Nastia.\nI live in Minsk.\n";
    }
}
